package org.sber.exercise2;

public interface ExecutionManager {
    Context execute(Runnable callback, Runnable... tasks) throws InterruptedException;
}


