package org.sber.exercise2;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ExecutionManagerImpl implements ExecutionManager {



    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        CountDownLatch countDownLatch = new CountDownLatch(tasks.length);
        List<Thread> threads = Arrays.stream(tasks).map(task -> new Thread(() -> {
                    try {
                        task.run();
                        countDownLatch.countDown();
                    } catch (Exception e) {
                        countDownLatch.countDown();
                        throw e;
                    }
                })
        ).collect(Collectors.toList());
        AtomicInteger failedTaskCount = new AtomicInteger();
        Context context = new ContextImpl( failedTaskCount, threads);
        new Thread(() -> {
            for (Thread thread : threads) {
                thread.setUncaughtExceptionHandler((t, e) -> failedTaskCount.incrementAndGet());
                thread.start();
            }
            try {
                countDownLatch.await();
                callback.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        return context;
    }
}
