package org.sber.exercise2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ContextImpl implements Context {


    private  AtomicInteger failedTaskCount;
    private  List<Thread> threads;

    private  List<Thread> interruptThreads;


    public ContextImpl(AtomicInteger failedTaskCount,
                       List<Thread> threads) {
        this.failedTaskCount = failedTaskCount;
        this.threads = threads;
    }

    @Override
    public int getCompletedTaskCount() {
        int count = 0;
        for (Thread thread : threads) {
            if (thread.getState() == Thread.State.TERMINATED) {
                count++;
            }
        }
        return count - failedTaskCount.get();
    }

    @Override
    public int getFailedTaskCount() {
        return failedTaskCount.get();
    }

    @Override
    public int getInterruptedTaskCount() {
        if (interruptThreads != null) {
            return interruptThreads.size();
        }
        return 0;
    }

    @Override
    public void interrupt() {
        interruptThreads = threads.stream().
                filter(thread -> thread.getState() == Thread.State.NEW)
                .collect(Collectors.toList());
        interruptThreads.forEach(Thread::interrupt);
    }

    @Override
    public boolean isFinished() {
       boolean flag = true;
        if (getCompletedTaskCount() == threads.size() ) {
            flag = true;
       } else {
            flag = false;
            if (interruptThreads != null && getInterruptedTaskCount() == threads.size() ) {
                flag = true;
            }
        }
       return flag;
    }


}
