package org.sber.exercise2;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Runnable callback = () -> System.out.println("callback");
        Runnable task1 = () -> {
            while (Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("task1");
            }
        } ;

        Runnable task2 = () -> {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("task2");
        };
        Runnable task3 = () ->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("task3");
        };
        Runnable task4 = () -> {
            throw new IllegalStateException();
        };



        ExecutionManagerImpl executionManager = new ExecutionManagerImpl();
        Context context = executionManager.execute(callback, task1, task2, task3, task4);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println(context.getCompletedTaskCount());
        System.out.println(context.getFailedTaskCount());
        context.interrupt();
        System.out.println(context.getInterruptedTaskCount());
        System.out.println(context.isFinished());

    }
}
