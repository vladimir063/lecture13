package org.sber.exercise1;

import java.util.concurrent.Callable;

public class Task<T> {

    private volatile Callable<? extends T> callable;
    private volatile T result;
    private volatile boolean isException;


    public Task(Callable<? extends T> callable) {
        this.callable = callable;
    }

    public T get()  {
        if (!isException) {
            if (result == null) {
                synchronized (this){
                    if (result == null) {
                        try {
                            result = callable.call();
                            return result;
                        } catch (Exception e) {
                            isException = true;
                            throw new GetResultException();
                        }
                    }
                }
            }
        } else{
            throw new GetResultException();
        }
        return result;
    }
}

