package org.sber.exercise1;

import java.util.Random;
import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {


    @Override
    public String call() throws Exception {
        return givenRandomString();
    }

    public String givenRandomString() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;

        return new Random().ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}

