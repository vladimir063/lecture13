package org.sber.exercise1;

public class Main {
    static String result1;
    static String result2;
    static Task task;

    public static void main(String[] args) throws Exception {
        while (true) {
            task = new Task(new MyCallable());
            Runnable runnable1 = () -> {
                result1 = (String) task.get();
             //   throw new IllegalStateException();
            };
            Thread.sleep(1000);
            Runnable runnable2 = () -> {
                result2 = (String) task.get();
            };

            Thread thread1 = new Thread(runnable1);
            thread1.start();
            Thread thread2 = new Thread(runnable2);
            thread2.start();

            thread1.join();
            thread2.join();

            if (!result1.equals(result2)) {
                throw new GetResultException();
            }
        }
    }
}


